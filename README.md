# Analyzers Common Library

This repository contains Go packages you may use to create analyzers.

## How to use the analyzers

Analyzers are shipped as Docker images.
All analyzers based on this library can be used the same way.
Here's an example using the
[find-sec-bugs](https://gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs) Docker image:

1. `cd` into the directory of the source code you want to scan
1. Run the Docker image:

    ```sh
    docker run \
      --interactive --tty --rm \
      --volume "$PWD":/tmp/app \
      --env CI_PROJECT_DIR=/tmp/app \
      registry.gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs:${VERSION} /analyzer run
    ```

    `VERSION` must be replaced with one of the available releases, see [versioning](#Versioning-and-release-process).

1. The Docker container generates a report in the mounted project directory:

    - for SAST: `gl-sast-report.json`
    - for Dependency Scanning: `gl-dependency-scanning-report.json`

## Analyzers development

To update the analyzer:

1. Modify the Go source code.
1. Compile a Linux binary.
1. Build a new Docker image.
1. Run the analyzer against its test project.
1. Compare the generated report with what's expected.

Here's how to create a Docker image named `analyzer`:

```sh
GOOS=linux go build -o analyzer
docker build -t analyzer .

docker run --rm \
  --volume "$PWD"/test/fixtures:/tmp/project \
  --env CI_PROJECT_DIR=/tmp/project \
  analyzer \
  /analyzer run
```

Then you can test it:

- for SAST analyzers:

    ```sh
    diff test/fixtures/gl-sast-report.json test/expect/gl-sast-report.json
    ```
- for Dependency Scanning analyzers:

    ```sh
    diff test/fixtures/gl-dependency-scanning-report.json test/expect/gl-dependency-scanning-report.json
    ```

You can also compile the binary for your own environment and run it locally
but `analyze` and `run` probably won't work
since the runtime dependencies of the analyzer are missing.

Here's an illustration based on
[find-sec-bugs](https://gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs):

```sh
go build -o analyzer
./analyzer search test/fixtures
./analyzer convert test/fixtures/app/spotbugsXml.Xml > ./gl-sast-report.json
```

## How to use the library

You can easily bootstrap a new analyzer project by copying
the [template](template) to a new repository and implementing the TODOs.

Analyzer relies on the [`command`](command) Go package to implement
a command line that implements these sub-commands:

- `search` searches for a project that is supported by the analyzer.
- `analyze` performs the analysis in a given directory.
- `convert` converts the output to a `gl-sast-report.json` artifact.
- `run` performs all the previous steps consecutively.

All you need to do is to implement:
- a match function that implements [`command.MatchFunc`](command/search.go)
- a conversion function that implements [`command.ConvertFunc`](command/convert.go)
- an analyze function and a function that lists the flags for the `analyze` sub-command

See [template/main.go](template/main.go)

## Versioning and release process

Analyzers are independent projects that follow their own versioning. Though to ensure compatibility between analyzers, common API and orchestrators that uses them, they all share the same `MAJOR` version number. `Minor` and `Patch` can be freely bumped as long as backward compatibility with the common API is kept. In case of breaking changes imposed by the wrapped scanner, creating a new analyzer on a separate repository must be considered.

The analyzers are released as Docker images following this scheme:

- each push to the `master` branch will override the `edge` image tag
- each push to any `awesome-feature` branch will generate a matching `awesome-feature` image tag
- each git tag will generate the corresponding `Major.Minor.Patch` image tag. A manual job allows to override the corresponding `Major` and the `latest` image tags to point to this `Major.Minor.Patch`. This will also override the configured `Major-Minor-stable` images (matching GitLab monthly releases) to point to this newer version.

When to trigger the manual job is still being discussed in https://gitlab.com/gitlab-org/gitlab-ee/issues/8369.

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the GitLab Enterprise Edition (EE) license, see the [LICENSE](LICENSE) file.
