package plugin

import (
	"sort"
	"sync"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/search"
)

var pluginsMu sync.Mutex
var plugins = make(map[string]search.MatchFunc)

// Register registers a match function with a given image name.
func Register(name string, plugin search.MatchFunc) {
	pluginsMu.Lock()
	defer pluginsMu.Unlock()
	if plugin == nil {
		panic("Register called with nil function for name " + name)
	}
	if _, dup := plugins[name]; dup {
		panic("Register called twice for name " + name)
	}
	plugins[name] = plugin
}

// Get retrieves the function associated with the given name.
func Get(name string) search.MatchFunc {
	pluginsMu.Lock()
	defer pluginsMu.Unlock()
	plugin := plugins[name]
	return plugin
}

// Names returns a sorted list of registered names.
func Names() []string {
	pluginsMu.Lock()
	defer pluginsMu.Unlock()
	var list []string
	for name := range plugins {
		list = append(list, name)
	}
	sort.Strings(list)
	return list
}
