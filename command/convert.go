package command

import (
	"encoding/json"
	"fmt"
	"io"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

type ConvertFunc func(input io.Reader, preprendPath string) (*issue.Report, error)

const flagPrependPath = "prepend-path"

// Search returns a cli sub-command that converts the analyzer output into an artifact.
func Convert(convert ConvertFunc) cli.Command {
	return cli.Command{
		Name:      "convert",
		Aliases:   []string{"c"},
		Usage:     "Convert analyzer output to a compatible artifact",
		ArgsUsage: "<input>",
		Flags: []cli.Flag{
			cli.StringFlag{
				Name:  flagPrependPath,
				Usage: "Path prepended to paths of affected files",
				Value: ".",
			},
		},
		Action: func(c *cli.Context) error {
			// check args
			if len(c.Args()) != 1 {
				cli.ShowSubcommandHelp(c)
				return errInvalidArgs
			}

			// open input file
			input := c.Args().First()
			reader, err := os.Open(input)
			if err != nil {
				return err
			}
			defer func() {
				if err := reader.Close(); err != nil {
					fmt.Fprintln(c.App.ErrWriter, err)
				}
			}()

			// convert output to issues
			issues, err := convert(reader, c.String(flagPrependPath))
			if err != nil {
				return err
			}

			// write JSON output
			enc := json.NewEncoder(c.App.Writer)
			enc.SetIndent("", "  ")
			return enc.Encode(issues)
		},
	}
}
