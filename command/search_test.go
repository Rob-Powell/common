package command

import (
	"bytes"
	"errors"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/urfave/cli"
)

var (
	lastExitCode = 0
	fakeOsExiter = func(rc int) {
		lastExitCode = rc
	}
	fakeErrWriter = &bytes.Buffer{}
)

func init() {
	cli.OsExiter = fakeOsExiter
	cli.ErrWriter = fakeErrWriter
}

func TestSearch(t *testing.T) {
	t.Run("found", func(t *testing.T) {
		var tcs = []struct {
			Filename   string
			WantOutput string
		}{
			{
				Filename:   "pom.xml",
				WantOutput: "fixtures/search",
			},
			{
				Filename:   "main.c",
				WantOutput: "fixtures/search/c/c",
			},
			{
				Filename:   "c",
				WantOutput: "fixtures/search/c",
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Filename, func(t *testing.T) {
				match := func(path string, info os.FileInfo) (bool, error) {
					return info.Name() == tc.Filename, nil
				}
				buf := new(bytes.Buffer)
				app := cli.NewApp()
				app.Name = "search"
				app.Usage = "Test search command"
				app.Writer = buf
				app.Commands = []cli.Command{Search(match)}
				err := app.Run([]string{"analyzer", "search", "fixtures/search"})
				if err != nil {
					t.Fatal(err)
				}
				got := strings.TrimSpace(buf.String())
				if got != tc.WantOutput {
					t.Errorf("Wrong output. Expected '%s' but got '%s'", tc.WantOutput, got)
				}
			})
		}
	})

	t.Run("match error", func(t *testing.T) {
		matchErr := errors.New("Error in match function")
		match := func(path string, info os.FileInfo) (bool, error) {
			return false, matchErr
		}
		app := cli.NewApp()
		app.Name = "search"
		app.Usage = "Test search command"
		app.Commands = []cli.Command{Search(match)}
		err := app.Run([]string{"analyzer", "search", "fixtures/search"})
		if err != matchErr {
			t.Fatalf("Expected error to be %v, but got: %v", err, matchErr)
		}
	})

	t.Run("not found", func(t *testing.T) {
		match := func(path string, info os.FileInfo) (bool, error) {
			return false, nil
		}
		app := cli.NewApp()
		app.Name = "search"
		app.Usage = "Test search command"
		app.Commands = []cli.Command{Search(match)}
		app.Run([]string{"analyzer", "search", "fixtures/search"})
		want, got := 3, lastExitCode
		if want != got {
			t.Fatalf("Expected exit code to be %v, but got: %v", want, got)
		}
	})

	t.Run("invalid args", func(t *testing.T) {
		match := func(path string, info os.FileInfo) (bool, error) {
			return false, nil
		}
		app := cli.NewApp()
		app.Name = "search"
		app.Usage = "Test search command"
		app.Commands = []cli.Command{Search(match)}
		app.Writer = ioutil.Discard
		app.Run([]string{"analyzer", "search"})
		want, got := 2, lastExitCode
		if want != got {
			t.Fatalf("Expected exit code to be %v, but got: %v", want, got)
		}
	})
}
