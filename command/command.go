package command

import (
	"io"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/search"
)

type AnalyzeFunc func(c *cli.Context, path string) (io.ReadCloser, error)

type Config struct {
	ArtifactName string           // Name of the generated artifact
	Match        search.MatchFunc // Match is a function that detects a compatible project.
	Analyze      AnalyzeFunc      // Analyze is a function that performs the analysis where a project was detected.
	AnalyzeFlags []cli.Flag       // AnalyzeFlags is a set command line options used by the analyze function (optional).
	AnalyzeAll   bool             // AnalyzeAll instructs the run command to analyze the root directory (false by default).
	Convert      ConvertFunc      // Convert is a function that turns the analyzer output into a compatible artifact.
}

func NewCommands(cfg Config) []cli.Command {
	if cfg.Match == nil {
		panic("Convert function not defined")
	}
	if cfg.Analyze == nil {
		panic("Analyze function not defined")
	}
	if cfg.AnalyzeFlags == nil {
		panic("Analyze flags not defined")
	}
	if cfg.Convert == nil {
		panic("Convert function not defined")
	}

	return []cli.Command{
		Run(cfg),
		Search(cfg.Match),
		Convert(cfg.Convert),
	}
}
