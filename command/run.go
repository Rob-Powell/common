package command

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/search"
)

const (
	ArtifactNameSAST               = "gl-sast-report.json"
	ArtifactNameDependencyScanning = "gl-dependency-scanning-report.json"

	EnvVarTargetDir    = "ANALYZER_TARGET_DIR"
	EnvVarArtifactDir  = "ANALYZER_ARTIFACT_DIR"
	EnvVarCIProjectDir = "CI_PROJECT_DIR"

	flagTargetDir   = "target-dir"
	flagArtifactDir = "artifact-dir"
)

func Run(cfg Config) cli.Command {
	flags := []cli.Flag{
		cli.StringFlag{
			Name:   flagTargetDir,
			Usage:  "Target directory",
			EnvVar: EnvVarTargetDir + "," + EnvVarCIProjectDir,
		},
		cli.StringFlag{
			Name:   flagArtifactDir,
			Usage:  "Artifact directory",
			EnvVar: EnvVarArtifactDir + "," + EnvVarCIProjectDir,
		},
	}
	flags = append(flags, search.NewFlags()...)
	flags = append(flags, cfg.AnalyzeFlags...)

	// Artifact name defaults to the name of SAST artifacts
	// to ensure backward compatibility with existing SAST analyzers.
	// There's no NewConfig() function so this is the best place to put it.
	artifactName := cfg.ArtifactName
	if artifactName == "" {
		artifactName = ArtifactNameSAST
	}

	return cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Run the analyzer on detected project and generate a compatible artifact",
		Flags:   flags,
		Action: func(c *cli.Context) error {
			// no args
			if c.Args().Present() {
				cli.ShowSubcommandHelp(c)
				return errInvalidArgs
			}

			// search directory
			root, err := filepath.Abs(c.String(flagTargetDir))
			if err != nil {
				return err
			}

			// search
			searchOpts := search.NewOptions(c)
			matchPath, err := search.New(cfg.Match, searchOpts).Run(root)
			if err != nil {
				return err
			}
			fmt.Fprintln(c.App.Writer, "Found project in "+matchPath)

			// target directory
			var target string
			if cfg.AnalyzeAll {
				// analyze the root directory
				target = root
			} else {
				// analyze the directory where there was a match
				target, err = filepath.Abs(matchPath)
				if err != nil {
					return err
				}
			}

			// analyze
			f, err := cfg.Analyze(c, target)
			if err != nil {
				return err
			}
			defer f.Close()

			// relative path of the target directory
			rel, err := filepath.Rel(root, target)
			if err != nil {
				return err
			}

			// convert
			issues, err := cfg.Convert(f, rel)
			if err != nil {
				return err
			}

			// write indented JSON to artifact
			artifactPath := filepath.Join(c.String(flagArtifactDir), artifactName)
			f2, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
			if err != nil {
				return err
			}
			defer f2.Close()
			enc := json.NewEncoder(f2)
			enc.SetIndent("", "  ")
			return enc.Encode(issues)
		},
	}
}
