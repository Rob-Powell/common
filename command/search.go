package command

import (
	"fmt"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/search"
)

// Search returns a cli sub-command that implements project search.
func Search(match search.MatchFunc) cli.Command {
	return cli.Command{
		Name:      "search",
		Aliases:   []string{"s"},
		Usage:     "Search for compatible projects and return project directory",
		ArgsUsage: "<path>",
		Flags:     search.NewFlags(),
		Action: func(c *cli.Context) error {
			// check args
			if len(c.Args()) != 1 {
				cli.ShowSubcommandHelp(c)
				return errInvalidArgs
			}
			rootPath := c.Args().First()

			// search
			opts := search.NewOptions(c)
			matchPath, err := search.New(match, opts).Run(rootPath)
			if err != nil {
				return err
			}
			fmt.Fprintln(c.App.Writer, matchPath)
			return nil
		},
	}
}
