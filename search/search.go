package search

import (
	"errors"
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/walk"
)

// MatchFunc is the interface of match functions.
type MatchFunc func(path string, info os.FileInfo) (bool, error)

// New returns a new Search initialized with given matching function and options.
func New(mf MatchFunc, opts *walk.Options) *Search {
	if opts == nil {
		opts = &walk.Options{}
	}
	return &Search{mf, opts}
}

// Search implements a search.
type Search struct {
	match   MatchFunc
	options *walk.Options
}

var abort = errors.New("abort search")

// Run performs a search on given path and returns matching path, otherwise an error.
func (s Search) Run(searchPath string) (string, error) {
	var matchPath string
	walkFunc := func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return nil
		}
		found, err1 := s.match(path, info)
		if found {
			if info.IsDir() {
				matchPath = path
			} else {
				matchPath = filepath.Dir(path)
			}
			return abort
		}
		return err1
	}

	switch err := walk.Walk(searchPath, walkFunc, s.options); err {
	case abort:
		return matchPath, nil
	case nil:
		return matchPath, ErrNotFound{searchPath}
	default:
		return matchPath, err
	}
}
