package issue

import (
	"crypto/sha1"
	"fmt"
	"io"
)

// Dedupe removes duplicates from a given list of issues.
// Duplicates shares the same location and at least one identifier.
// CWE ids are ignored since these are used to classify the vulnerability.
// First duplicate in the list wins and others are simply removed.
func Dedupe(issues ...Issue) []Issue {
	type keyType struct {
		locSHA1 string         // SHA1 of string representation of location
		idType  IdentifierType // identifier type
		idValue string         // identifier value
	}

	var seen = make(map[keyType]bool) // keys that have been seen already
	var out = make([]Issue, 0)        // issues that are returned

	for _, issue := range issues {
		// turn location into a SHA1
		h := sha1.New()
		io.WriteString(h, fmt.Sprintf("%#v", issue.Location))
		locSHA1 := fmt.Sprintf("%x", h.Sum(nil))

		// iterate over identifiers
		var alreadySeen bool
	innerLoop:
		for _, id := range issue.Identifiers {
			switch id.Type {
			case IdentifierTypeCWE:
				// ignored
			default:
				var key = keyType{locSHA1, id.Type, id.Value}
				if _, found := seen[key]; found {
					alreadySeen = true
					break innerLoop
				}
				seen[key] = true
			}
		}
		if !alreadySeen {
			out = append(out, issue)
		}
	}

	return out
}
