package issue

import (
	"reflect"
	"testing"
)

func TestDedupe(t *testing.T) {

	// nokogiri CVE-2018-14404, USN-3739-1 via app/Gemfile.lock (dup)
	var issue = Issue{
		Location: Location{
			File: "app/Gemfile.lock",
			Dependency: Dependency{
				Package: Package{
					Name: "nokogiri",
				},
			},
		},
		Identifiers: []Identifier{
			USNIdentifier("USN-3739-1"),
			CVEIdentifier("CVE-2018-14404"),
		},
	}

	// nokogiri CVE-2018-14404 via app/Gemfile.lock (dup)
	var issueDup = Issue{
		Location: Location{
			File: "app/Gemfile.lock",
			Dependency: Dependency{
				Package: Package{
					Name: "nokogiri",
				},
			},
		},
		Identifiers: []Identifier{
			CVEIdentifier("CVE-2018-14404"),
		},
	}

	// nokogiri USN-3739-1 via app/Gemfile.lock (dup)
	var issueDup2 = Issue{
		Location: Location{
			File: "app/Gemfile.lock",
			Dependency: Dependency{
				Package: Package{
					Name: "nokogiri",
				},
			},
		},
		Identifiers: []Identifier{
			USNIdentifier("USN-3739-1"),
		},
	}

	// nokogiri CVE-2018-14404, USN-3739-1 via other/Gemfile.lock
	var issueOtherFile = Issue{
		Location: Location{
			File: "other/Gemfile.lock",
			Dependency: Dependency{
				Package: Package{
					Name: "nokogiri",
				},
			},
		},
		Identifiers: []Identifier{
			USNIdentifier("USN-3739-1"),
			CVEIdentifier("CVE-2018-14404"),
		},
	}

	// libxml CVE-2018-14404, USN-3739-1 via app/Gemfile.lock
	var issueOtherPackage = Issue{
		Location: Location{
			File: "app/Gemfile.lock",
			Dependency: Dependency{
				Package: Package{
					Name: "libxml",
				},
			},
		},
		Identifiers: []Identifier{
			USNIdentifier("USN-3739-1"),
			CVEIdentifier("CVE-2018-14404"),
		},
	}

	// nokogiri USN-3271-1 via app/Gemfile.lock
	var issueOtherId = Issue{
		Location: Location{
			File: "app/Gemfile.lock",
			Dependency: Dependency{
				Package: Package{
					Name: "nokogiri",
				},
			},
		},
		Identifiers: []Identifier{
			USNIdentifier("USN-3271-1"),
		},
	}

	var input = []Issue{
		issue,
		issueOtherFile,
		issueDup,
		issueDup2,
		issueOtherPackage,
		issueOtherId,
	}

	var want = []Issue{
		issue,
		issueOtherFile,
		issueOtherPackage,
		issueOtherId,
	}

	got := Dedupe(input...)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expecting:\n%v\nGot:\n%v", want, got)
	}
}
