package issue

type Remediation struct {
	Fixes   []IssueRef `json:"fixes"`   // Refs to fixed vulnerabilities
	Summary string     `json:"summary"` // Overview of how the vulnerabilities have been fixed
	Diff    string     `json:"diff"`    // Base64 encoded diff, compatible with "git apply"
}

type IssueRef struct {
	CompareKey string `json:"cve"`
}
