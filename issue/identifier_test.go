package issue

import (
	"reflect"
	"testing"
)

func TestIdentifier(t *testing.T) {
	var tcs = []struct {
		Name  string
		Parse string // argument given to ParseIdentifier
		Got   Identifier
		Want  Identifier
	}{
		{
			Name:  "CVEIdentifier",
			Parse: "CVE-123",
			Got:   CVEIdentifier("CVE-123"),
			Want: Identifier{
				Type:  "cve",
				Name:  "CVE-123",
				Value: "CVE-123",
				URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-123",
			},
		},
		{
			Name:  "CWEIdentifier",
			Parse: "CWE-123",
			Got:   CWEIdentifier(123),
			Want: Identifier{
				Type:  "cwe",
				Name:  "CWE-123",
				Value: "123",
				URL:   "https://cwe.mitre.org/data/definitions/123.html",
			},
		},
		{
			Name:  "OSVDBIdentifier",
			Parse: "OSVDB-123",
			Got:   OSVDBIdentifier("OSVDB-123"),
			Want: Identifier{
				Type:  "osvdb",
				Name:  "OSVDB-123",
				Value: "OSVDB-123",
				URL:   "https://cve.mitre.org/data/refs/refmap/source-OSVDB.html",
			},
		},
		{
			Name:  "USNIdentifier",
			Parse: "USN-123",
			Got:   USNIdentifier("USN-123"),
			Want: Identifier{
				Type:  "usn",
				Name:  "USN-123",
				Value: "USN-123",
				URL:   "https://usn.ubuntu.com/123/",
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.Name, func(t *testing.T) {
			if !reflect.DeepEqual(tc.Got, tc.Want) {
				t.Errorf("Wrong result. Expected:\n%#v\nBut got:\n%#v", tc.Want, tc.Got)
			}
		})
	}

	t.Run("ParseIdentifier", func(t *testing.T) {
		for _, tc := range tcs {
			got, ok := ParseIdentifierID(tc.Parse)
			if !ok {
				t.Errorf("Expected %s to be parsed successfully", tc.Parse)
			}
			if !reflect.DeepEqual(got, tc.Want) {
				t.Errorf("Wrong result. Expected:\n%#v\nBut got:\n%#v", tc.Want, got)
			}
		}
	})
}
