package issue

import (
	"encoding/json"
	"strings"
)

type Issue struct {
	Category    Category     `json:"category"`              // Category describes where this vulnerability belongs (SAST, Dependency Scanning, etc...)
	Name        string       `json:"name,omitempty"`        // Name of the vulnerability, this must not include occurence's specific information.
	Message     string       `json:"message,omitempty"`     // Message is a short text that describes the vulnerability, it may include occurence's specific information.
	Description string       `json:"description,omitempty"` // Description is a long text that describes the vulnerability.
	CompareKey  string       `json:"cve"`                   // CompareKey is a value used to establish whether two issues are the same. Not reliable!
	Severity    Level        `json:"severity,omitempty"`    // Severity describes how much the vulnerability impacts the software.
	Confidence  Level        `json:"confidence,omitempty"`  // Confidence describes how the vulnerability is likely to impact the software.
	Solution    string       `json:"solution,omitempty"`    // Solution explains how to fix the vulnerability.
	Scanner     Scanner      `json:"scanner"`               // Scanner identifies the analyzer.
	Location    Location     `json:"location"`              // Location tells which class and/or method is affected by the vulnerability.
	Identifiers []Identifier `json:"identifiers"`           // Identifiers are references that identify a vulnerability on internal or external DBs.
	Links       []Link       `json:"links,omitempty"`       // Links are external documentations or articles that further describes the vulnerability.
}

type Category string

const (
	CategorySast               = "sast"
	CategoryDependencyScanning = "dependency_scanning"
	CategoryContainerScanning  = "container_scanning"
	CategoryDast               = "dast"
)

// Level type used by severity and confidence values.
// Please note that not every levels are relevant for both properties.
type Level int

const (
	LevelUndefined Level = iota
	LevelIgnore
	LevelUnknown
	LevelExperimental
	LevelLow
	LevelMedium
	LevelHigh
	LevelCritical
)

// MarshalJSON is used to convert a Level object into a JSON representation
func (l Level) MarshalJSON() ([]byte, error) {
	return json.Marshal(l.String())
}

// UnmarshalJSON is used to convert a JSON representation into a Level object
func (l *Level) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	*l = ParseLevel(s)
	return nil
}

// ParseLevel converts a string to a Level
func ParseLevel(s string) Level {
	switch strings.ToLower(s) {
	case "critical":
		return LevelCritical
	case "high":
		return LevelHigh
	case "medium":
		return LevelMedium
	case "low":
		return LevelLow
	case "experimental":
		return LevelExperimental
	case "unknown":
		return LevelUnknown
	case "ignore":
		return LevelIgnore
	default:
		return LevelUndefined
	}
}

// String converts a Level into a string
func (l Level) String() string {
	switch l {
	case LevelCritical:
		return "Critical"
	case LevelHigh:
		return "High"
	case LevelMedium:
		return "Medium"
	case LevelLow:
		return "Low"
	case LevelExperimental:
		return "Experimental"
	case LevelUnknown:
		return "Unknown"
	case LevelIgnore:
		return "Ignore"
	}
	return ""
}

type Location struct {
	File       string `json:"file,omitempty"`       // File is the path relative to the search path.
	LineStart  int    `json:"start_line,omitempty"` // LineStart is the first line of the affected code.
	LineEnd    int    `json:"end_line,omitempty"`   // LineEnd is the last line of the affected code.
	Class      string `json:"class,omitempty"`
	Method     string `json:"method,omitempty"`
	Dependency `json:"dependency,omitempty"`
}

type Dependency struct {
	Package `json:"package,omitempty"`
	Version string `json:"version,omitempty"`
}

type Package struct {
	Name string `json:"name,omitempty"`
}

// NewLinks generate new links from URLs
func NewLinks(urls ...string) []Link {
	var links = make([]Link, len(urls))
	for i, url := range urls {
		links[i].URL = url
	}
	return links
}

type Link struct {
	Name string `json:"name,omitempty"` // Name of the link (optional)
	URL  string `json:"url"`            // URL of the document (mandatory)
}

type Scanner struct {
	ID   string `json:"id"`   // Id of the scanner as a snake_case string (mandatory)
	Name string `json:"name"` // Name of the scanner, for display purpose (mandatory)
}
