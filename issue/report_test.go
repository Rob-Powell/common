package issue

import (
	"bytes"
	"encoding/json"
	"reflect"
	"testing"
)

var testReport = Report{
	Version: Version{2, 0},
	Vulnerabilities: []Issue{
		{
			Category:   CategoryDependencyScanning,
			Message:    "Vulnerability in io.netty/netty",
			CompareKey: "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
			Scanner: Scanner{
				ID:   "gemnasium",
				Name: "Gemnasium",
			},
			Location: Location{
				File: "app/pom.xml",
				Dependency: Dependency{
					Package: Package{
						Name: "io.netty/netty",
					},
					Version: "3.9.1.Final",
				},
			},
			Identifiers: []Identifier{
				CVEIdentifier("CVE-2018-1234"),
			},
		},
	},
	Remediations: []Remediation{
		{
			Fixes:   []IssueRef{{CompareKey: "app/pom.xml:io.netty/netty:cve:CVE-2018-1234"}},
			Summary: "Upgrade to netty 3.9.2.Final",
			Diff:    "diff (base64 encoded) placeholder",
		},
	},
}

var testReportJSON string = `{
  "version": "2.0",
  "vulnerabilities": [
    {
      "category": "dependency_scanning",
      "message": "Vulnerability in io.netty/netty",
      "cve": "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "io.netty/netty"
          },
          "version": "3.9.1.Final"
        }
      },
      "identifiers": [
        {
          "type": "cve",
          "name": "CVE-2018-1234",
          "value": "CVE-2018-1234",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
        }
      ]
    }
  ],
  "remediations": [
    {
      "fixes": [
        {
          "cve": "app/pom.xml:io.netty/netty:cve:CVE-2018-1234"
        }
      ],
      "summary": "Upgrade to netty 3.9.2.Final",
      "diff": "diff (base64 encoded) placeholder"
    }
  ]
}`

func TestReport(t *testing.T) {
	t.Run("MarshalJSON", func(t *testing.T) {
		b, err := json.Marshal(testReport)
		if err != nil {
			t.Fatal(err)
		}

		var buf bytes.Buffer
		json.Indent(&buf, b, "", "  ")
		got := buf.String()

		want := testReportJSON
		if got != want {
			t.Errorf("Wrong JSON output. Expected:\n%s\nBut got:\n%s", want, got)
		}
	})

	t.Run("UnmarshalJSON", func(t *testing.T) {
		var got Report
		if err := json.Unmarshal([]byte(testReportJSON), &got); err != nil {
			t.Fatal(err)
		}

		want := testReport
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Wrong unmarshalled value. Expected:\n%#v\nBut got:\n%#v", want, got)
		}
	})
}

func TestEmptyReport(t *testing.T) {
	var emptyReport = NewReport()
	var emptyReportJSON string = `{
  "version": "2.0",
  "vulnerabilities": [],
  "remediations": []
}`

	t.Run("MarshalJSON", func(t *testing.T) {
		b, err := json.Marshal(emptyReport)
		if err != nil {
			t.Fatal(err)
		}

		var buf bytes.Buffer
		json.Indent(&buf, b, "", "  ")
		got := buf.String()

		want := emptyReportJSON
		if got != want {
			t.Errorf("Wrong JSON output. Expected:\n%s\nBut got:\n%s", want, got)
		}
	})

	t.Run("UnmarshalJSON", func(t *testing.T) {
		var got Report
		if err := json.Unmarshal([]byte(emptyReportJSON), &got); err != nil {
			t.Fatal(err)
		}

		want := emptyReport
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Wrong unmarshalled value. Expected:\n%#v\nBut got:\n%#v", want, got)
		}
	})
}

func TestNewReport(t *testing.T) {
	got := NewReport().Version
	want := CurrentVersion()
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong version. Expecting %v but got %v", want, got)
	}
}

// NOTE: Sort is tested when testing MergeReports

func TestMergeReports(t *testing.T) {
	location := Location{File: "app/Gemfile.lock"}
	ids := []Identifier{CVEIdentifier("CVE-2018-14404")}

	reports := []Report{
		{
			Version: Version{Major: 2, Minor: 3},
			Vulnerabilities: []Issue{
				{
					Name:       "R1/low",
					Severity:   LevelLow,
					CompareKey: "R1/low/ckey",
				},
				{
					Name:        "R1/critical",
					Severity:    LevelCritical,
					Location:    location,
					Identifiers: ids,
					CompareKey:  "R1/critical/ckey",
				},
			},
			Remediations: []Remediation{
				{
					Fixes:   []IssueRef{{CompareKey: "R1/critical/ckey"}},
					Summary: "Upgrade dependency to fix R1/critical",
					Diff:    "diff fixing R1/critical",
				},
			},
		},
		{
			Version: Version{Major: 2, Minor: 4},
			Vulnerabilities: []Issue{
				{
					Name:       "R2/low",
					Severity:   LevelLow,
					CompareKey: "R2/low/ckey",
				},
				{
					Name:       "R2/critical",
					Severity:   LevelCritical,
					CompareKey: "R2/critical/ckey",
				},
			},
		},
		{
			Version: Version{Major: 2, Minor: 5},
			Vulnerabilities: []Issue{
				{
					Name:       "R3/low",
					Severity:   LevelLow,
					CompareKey: "R3/low/ckey",
				},
				{
					Name:       "R3/critical",
					Severity:   LevelCritical,
					CompareKey: "R3/critical/ckey",
				},
				{
					Name:        "R3/critical/dup",
					Severity:    LevelCritical,
					Location:    location,
					Identifiers: ids,
					CompareKey:  "R3/critical/ckey",
				},
			},
			Remediations: []Remediation{
				{
					Fixes:   []IssueRef{{CompareKey: "R3/critical/ckey"}},
					Summary: "Upgrade dependency to fix R3/critical",
					Diff:    "diff fixing R3/critical",
				},
			},
		},
	}
	want := Report{
		Version: CurrentVersion(),
		Vulnerabilities: []Issue{
			{
				Name:        "R1/critical",
				Severity:    LevelCritical,
				Location:    location,
				Identifiers: ids,
				CompareKey:  "R1/critical/ckey",
			},
			{
				Name:       "R2/critical",
				Severity:   LevelCritical,
				CompareKey: "R2/critical/ckey",
			},
			{
				Name:       "R3/critical",
				Severity:   LevelCritical,
				CompareKey: "R3/critical/ckey",
			},
			{
				Name:       "R1/low",
				Severity:   LevelLow,
				CompareKey: "R1/low/ckey",
			},
			{
				Name:       "R2/low",
				Severity:   LevelLow,
				CompareKey: "R2/low/ckey",
			},
			{
				Name:       "R3/low",
				Severity:   LevelLow,
				CompareKey: "R3/low/ckey",
			},
		},
		Remediations: []Remediation{
			{
				Fixes:   []IssueRef{{CompareKey: "R1/critical/ckey"}},
				Summary: "Upgrade dependency to fix R1/critical",
				Diff:    "diff fixing R1/critical",
			},
			{
				Fixes:   []IssueRef{{CompareKey: "R3/critical/ckey"}},
				Summary: "Upgrade dependency to fix R3/critical",
				Diff:    "diff fixing R3/critical",
			},
		},
	}

	got := MergeReports(reports...)
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nGot:\n%#v", want, got)
	}
}
