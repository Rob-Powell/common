# TODO analyzer

This analyzer is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Versioning and release process

Please check the common [Versioning and release process documentation](https://gitlab.com/gitlab-org/security-products/analyzers/common/blob/update_doc/README.md#versioning-and-release-process).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the GitLab Enterprise Edition (EE) license, see 
the [LICENSE](LICENSE) file.
