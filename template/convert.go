package main

import (
	"encoding/xml"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

const toolID = "XXX" // TODO

func convert(reader io.Reader, prependPath string) (*issue.Report, error) {
	var doc Document

	err := xml.NewDecoder(reader).Decode(&doc)
	if err != nil {
		return nil, err
	}

	issues := make([]issue.Issue, len(doc.Messages)) // TODO
	for i, msg := range doc.Messages {
		issues[i] = issue.Issue{
			Message: msg, // TODO
		}
	}

	report := issue.NewReport()
	report.Vulnerabilities = issues
	return &report, nil
}

type Document struct {
	Messages []string `xml:"XXX"` // TODO
}
