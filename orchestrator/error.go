package orchestrator

import "errors"

// ErrNotFound is raised when no compatible analyzer can be found.
var ErrNotFound = errors.New("Project not found")

// ErrNonZeroStatus is raised when an analyzer fails.
var ErrNonZeroStatus = errors.New("Container exited with non zero status code")

// ErrNoArtifact is raised when an analyzer doesn't produce the expected artifact.
var ErrNoArtifact = errors.New("Artifact not found")

// ErrPullTimeout is raised if there's a timeout when pulling a Docker image.
var ErrPullTimeout = errors.New("Timeout when pulling the Docker image")

// ErrRunTimeout is raised if there's a timeout when running an analyzer
var ErrRunTimeout = errors.New("Timeout when running the analyzer")

// ErrNegotiateTimeout is raised if there's a timeout when connecting to the Docker server.
var ErrNegotiateTimeout = errors.New("Timeout when connecting to Docker server")

var errNoCompatibleAnalyzer = &ErrNoCompatibleAnalyzer{}

// ErrNoCompatibleAnalyzer is raised when no compatible analyzer can be found.
type ErrNoCompatibleAnalyzer struct{}

// Error returns the error message.
func (e ErrNoCompatibleAnalyzer) Error() string {
	return "No compatible analyzer can be found"
}

// ExitCode returns the exit code of the command line.
func (e ErrNoCompatibleAnalyzer) ExitCode() int {
	return 3
}

var errEmptyProjectDirectory = &ErrEmptyProjectDirectory{}

// ErrEmptyProjectDirectory is raised when the project directory is empty
type ErrEmptyProjectDirectory struct{}

// Error returns the error message.
func (e ErrEmptyProjectDirectory) Error() string {
	return "Project directory is empty"
}

// ExitCode returns the exit code of the command line.
func (e ErrEmptyProjectDirectory) ExitCode() int {
	return 4
}

var errNoAnalyzerImage = &ErrNoAnalyzerImage{}

// ErrNoAnalyzerImage is raised when there's no Docker image to analyze the project.
type ErrNoAnalyzerImage struct{}

// Error returns the error message.
func (e ErrNoAnalyzerImage) Error() string {
	return "No Docker image to analyze the project"
}

// ExitCode returns the exit code of the command line.
func (e ErrNoAnalyzerImage) ExitCode() int {
	return 5
}
