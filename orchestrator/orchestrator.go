package orchestrator

import (
	"context"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/docker/docker/client"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/search"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/walk"
)

const (
	imageFindSecBugsGradle = "find-sec-bugs-gradle"
	imageFindSecBugsGroovy = "find-sec-bugs-groovy"
)

// Config configures the orchestrator.
type Config struct {
	ProjectDir       string
	PullEnabled      bool
	Images           []string
	ArtifactName     string
	CopyProjectDir   bool
	SearchOpts       *walk.Options
	NegotiateTimeout time.Duration
	PullTimeout      time.Duration
	RunTimeout       time.Duration
}

// NewOrchestrator creates a new orchestrator.
func NewOrchestrator(config *Config) *Orchestrator {
	return &Orchestrator{config}
}

// Orchestrator is responsible for running all the analyzers on a given project directory.
type Orchestrator struct {
	*Config
}

// Run runs all the compatible analyzers on the project directory and merge the reports.
func (o Orchestrator) Run() (*issue.Report, error) {
	var reports []issue.Report

	// Project directory check, it must not be empty
	dir, err := os.Open(o.ProjectDir)
	if err != nil {
		return nil, err
	}
	defer dir.Close()

	_, err = dir.Readdirnames(1)
	if err == io.EOF {
		return nil, errEmptyProjectDirectory
	}

	// Docker client
	cli, err := client.NewEnvClient()
	if err != nil {
		return nil, err
	}

	// Negotiate API version
	ctx, cancel := context.WithTimeout(context.Background(), o.NegotiateTimeout)
	defer cancel()
	ping, err := cli.Ping(ctx)
	switch err {
	case nil:
		// OK
	case context.DeadlineExceeded:
		return nil, ErrNegotiateTimeout
	default:
		return nil, err

	}
	cli.NegotiateAPIVersionPing(ping)

	// Iterate over available images
	var compatFound bool
	for _, image := range o.Images {

		// Plugin name
		pname := strings.SplitN(filepath.Base(image), ":", 2)[0]
		logPrefix := "[" + pname + "] "

		// Attempt to check compatibility using plugin
		if match := plugin.Get(pname); match != nil {
			log.Println(logPrefix + "Detect project using plugin")
			_, err := search.New(match, o.SearchOpts).Run(o.ProjectDir)
			if _, ok := err.(search.ErrNotFound); ok {
				log.Printf(logPrefix + "Project not compatible")
				continue
			}
			if err != nil {
				return nil, err
			}
			log.Printf(logPrefix + "Project is compatible")
		} else {
			log.Printf(logPrefix + "No detection plugin named")
		}

		// New analyzer
		log.Printf(logPrefix + "Starting analyzer...")
		opts := AnalyzerOptions{}
		switch pname {
		case imageFindSecBugsGradle, imageFindSecBugsGroovy:
			/* HACK: force uid and gid when copying to find-sec-bugs-gradle.
			This is needed because:
			- Default user for this imagae is gradle, uid 1000.
			- The gradle command must run with the gradle user.
			- The gradle command requires write access to the project directory.
			- There's no way to know the user uid by inspecting the container.
			- We run the default command of the image, so we can't run "whoami".
			*/
			opts.tarOptions = tarOptions{SetOwner: true, UID: 1000, GID: 1000}
		}
		if o.CopyProjectDir {
			opts.ImportStrategy = ImportCopy
		}
		analyzer := NewAnalyzer(cli, image, o.ArtifactName, opts)

		// Pull Docker image
		pullCtx, cancel := context.WithTimeout(context.Background(), o.PullTimeout)
		defer cancel()

		if o.PullEnabled {
			switch err := analyzer.Pull(pullCtx); err {
			case nil:
				// OK
			case context.DeadlineExceeded:
				return nil, ErrPullTimeout
			default:
				return nil, err
			}
		}

		// Run analyzer and parse artifact
		runCtx, cancel := context.WithTimeout(context.Background(), o.RunTimeout)
		defer cancel()

		switch report, err := analyzer.Run(runCtx, o.ProjectDir); err {
		case nil:
			// merge reports
			reports = append(reports, *report)
			compatFound = true

		case context.DeadlineExceeded:
			return nil, ErrRunTimeout

		case ErrNotFound:
			// ignore

		default:
			// fail even if other analyzers work fine
			return nil, err
		}
	}

	// Fail if no compatible analyzer
	if !compatFound {
		return nil, errNoCompatibleAnalyzer
	}

	// Return merged reports
	merged := issue.MergeReports(reports...)
	return &merged, nil
}
