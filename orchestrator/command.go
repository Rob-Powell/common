package orchestrator

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/search"
)

const (
	flagPullImages              = "pull-images"
	flagDefaultAnalyzers        = "default-analyzer"
	flagDefaultAnalyzersEnabled = "default-analyzers-enabled"
	flagAnalyzerImages          = "image"
	flagAnalyzersImagePrefix    = "image-prefix"
	flagAnalyzersImageTag       = "image-tag"
	flagDisableRemoteCheck      = "disable-remote-check"

	flagNegotiateTimeout = "negotiate-timeout"
	flagPullTimeout      = "pull-timeout"
	flagRunTimeout       = "run-timeout"

	defaultTimeoutNegotiate = 2 * time.Minute
	defaultTimeoutPull      = 5 * time.Minute
	defaultTimeoutRun       = 20 * time.Minute

	envVarProjectDir = "CI_PROJECT_DIR"

	analyzersDefaultImagePrefix = "registry.gitlab.com/gitlab-org/security-products/analyzers"
	analyzersDefaultImageTag    = "latest"
)

type Options struct {
	EnvVarPrefix string
	ArtifactName string
	PostWrite    func(issue.Report)
}

func MakeFlags(opts Options) []cli.Flag {
	prefix := opts.EnvVarPrefix
	flags := []cli.Flag{
		// images
		cli.BoolTFlag{
			Name:   flagPullImages,
			EnvVar: prefix + "PULL_ANALYZER_IMAGES,PULL_IMAGES",
			Usage:  "Pull Docker images",
		},
		cli.StringSliceFlag{
			Name:   flagDefaultAnalyzers,
			EnvVar: prefix + "DEFAULT_ANALYZERS",
			Usage:  "Official/default analyzers",
		},
		cli.BoolTFlag{
			Name:   flagDefaultAnalyzersEnabled,
			EnvVar: prefix + "DEFAULT_ANALYZERS_ENABLED,DEFAULT_IMAGES",
			Usage:  "[deprecated] Enable default Docker images",
		},
		cli.StringSliceFlag{
			Name:   flagAnalyzerImages,
			EnvVar: prefix + "ANALYZER_IMAGES,ANALYZER_IMAGES",
			Usage:  "Docker images of custom analyzers",
		},
		cli.StringFlag{
			Name:   flagAnalyzersImagePrefix,
			EnvVar: prefix + "ANALYZER_IMAGE_PREFIX",
			Usage:  "Docker registry hosting the default analyzer images",
			Value:  analyzersDefaultImagePrefix,
		},
		cli.StringFlag{
			Name:   flagAnalyzersImageTag,
			EnvVar: prefix + "ANALYZER_IMAGE_TAG",
			Usage:  "Tag of the default analyzer images",
			Value:  analyzersDefaultImageTag,
		},
		cli.BoolFlag{
			Name:   flagDisableRemoteCheck,
			EnvVar: "DS_DISABLE_REMOTE_CHECKS,DEP_SCAN_DISABLE_REMOTE_CHECKS,SAST_DISABLE_REMOTE_CHECKS",
			Usage:  "Do not send any data to GitLab",
		},

		// timeouts
		cli.DurationFlag{
			Name:   flagNegotiateTimeout,
			EnvVar: prefix + "DOCKER_CLIENT_NEGOTIATION_TIMEOUT",
			Usage:  "Time limit for Docker client negotiation",
			Value:  defaultTimeoutNegotiate,
		},
		cli.DurationFlag{
			Name:   flagPullTimeout,
			EnvVar: prefix + "PULL_ANALYZER_IMAGE_TIMEOUT",
			Usage:  "Time limit when pulling the image of an analyzer",
			Value:  defaultTimeoutPull,
		},
		cli.DurationFlag{
			Name:   flagRunTimeout,
			EnvVar: prefix + "RUN_ANALYZER_TIMEOUT",
			Usage:  "Time limit when running an analyzer",
			Value:  defaultTimeoutRun,
		},
	}
	flags = append(flags, search.NewFlags()...)
	return flags
}

func MakeAction(opts Options) cli.ActionFunc {
	return func(c *cli.Context) error {
		// Parse cli flags and arguments
		cfg, err := ParseConfig(c, opts)
		if err != nil {
			return err
		}

		// Run orchestrator
		o := NewOrchestrator(cfg)
		report, err := o.Run()
		if err != nil {
			return err
		}

		// Write artifact
		artifactPath := filepath.Join(cfg.ProjectDir, opts.ArtifactName)
		if err := writeArtifact(*report, artifactPath); err != nil {
			return err
		}

		// Trigger post-write hook
		if opts.PostWrite != nil {
			opts.PostWrite(*report)
		}

		return nil
	}
}

// ParseConfig reads the cli context and returns an orchestrator configuration.
func ParseConfig(c *cli.Context, opts Options) (*Config, error) {
	cfg := &Config{ArtifactName: opts.ArtifactName}
	p := ConfigParser{cfg, opts}

	if err := p.Parse(c); err != nil {
		return nil, err
	}

	return cfg, nil
}

// ConfigParser is used to extract the orchestrator config from the cli context.
type ConfigParser struct {
	*Config
	Options
}

// Parse parses the cli context and updates the configuration.
func (cfg *ConfigParser) Parse(c *cli.Context) error {
	// Check arguments
	if len(c.Args()) > 2 {
		cli.ShowSubcommandHelp(c)
		return errors.New("Wrong number of arguments")
	}

	// Set project directory
	if c.Args().Present() {
		// Backward compatibility mode.
		log.Println("Copy project directory to containers")
		cfg.ProjectDir = c.Args().First()
		cfg.CopyProjectDir = true
	} else {
		// Mount $CI_PROJECT_DIR in containers.
		log.Println("Mount project directory in containers")
		if dir := os.Getenv(envVarProjectDir); dir == "" {
			log.Fatal(envVarProjectDir + " not set")
		} else {
			cfg.ProjectDir = dir
			cfg.CopyProjectDir = false
		}
	}

	// Deprecation warning
	if c.IsSet(flagDefaultAnalyzersEnabled) {
		withPrefix := func(name string) string {
			return cfg.EnvVarPrefix + name
		}
		log.Println("Warning!",
			withPrefix("DEFAULT_ANALYZERS_ENABLED"), "is now deprecated.",
			"Please use", withPrefix("DEFAULT_ANALYZERS"), "instead.")
	}

	// Parse default and custom images
	images := append(defaultImages(c), c.StringSlice(flagAnalyzerImages)...)
	if len(images) == 0 {
		return errNoAnalyzerImage
	}
	cfg.Images = images

	// Parse timeouts
	cfg.NegotiateTimeout = c.Duration(flagNegotiateTimeout)
	cfg.PullTimeout = c.Duration(flagPullTimeout)
	cfg.RunTimeout = c.Duration(flagRunTimeout)

	// Parse remaining flags
	cfg.SearchOpts = search.NewOptions(c)
	cfg.PullEnabled = c.BoolT(flagPullImages)

	return nil
}

// defaultImages returns the Docker images of the default analyzers configured in the command line.
func defaultImages(c *cli.Context) []string {
	// Build list of default/official analyzers
	analyzers := []string{}
	if c.IsSet(flagDefaultAnalyzers) {
		// NOTE: The slice contains one empty string if the environment variable
		// is set to an empty string; this is how users disable default analyzers.
		for _, analyzer := range c.StringSlice(flagDefaultAnalyzers) {
			if analyzer != "" {
				analyzers = append(analyzers, analyzer)
			}
		}
	} else {
		if c.BoolT(flagDefaultAnalyzersEnabled) { // NOTE: deprecated, will always be true
			analyzers = plugin.Names()
		}
	}

	// Filter gemnasium-based analyzers if remote checks are disabled
	if c.Bool(flagDisableRemoteCheck) {
		analyzers = filterPrefix(analyzers, "gemnasium")
	}

	// Turn analyzer names into Docker images
	prefix := c.String(flagAnalyzersImagePrefix)
	tag := c.String(flagAnalyzersImageTag)
	images := []string{}
	for _, analyzer := range analyzers {
		url := fmt.Sprintf("%s/%s:%s", prefix, analyzer, tag)
		images = append(images, url)
	}
	return images
}

// filterPrefix removes strings having given prefix.
func filterPrefix(in []string, prefix string) []string {
	var out []string
	for _, s := range in {
		if !strings.HasPrefix(s, prefix) {
			out = append(out, s)
		}
	}
	return out
}

// writeArtifact write the issue to the given path.
func writeArtifact(report issue.Report, path string) error {
	out, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer func() {
		if err := out.Close(); err != nil {
			log.Println(err)
		}
	}()
	enc := json.NewEncoder(out)
	enc.SetIndent("", "  ")
	if err := enc.Encode(report); err != nil {
		return err
	}
	return nil
}
