package orchestrator

import (
	"archive/tar"
	"io"
	"os"
	"path/filepath"
	"strings"
)

// tarOptions configures how the tar archive is created.
type tarOptions struct {
	SetOwner bool
	UID      int
	GID      int
}

// writeTar walks through srcPath and writes each file it founds to the writer.
// dstPath is used as a prefix for all the files of the archive.
func writeTar(srcPath, dstPath string, writer io.Writer, opts tarOptions) error {
	tw := tar.NewWriter(writer)
	defer tw.Close()

	// walk path
	return filepath.Walk(srcPath, func(file string, fi os.FileInfo, err error) error {

		// return on any error
		if err != nil {
			return err
		}

		// return on non-regular files or directories
		if !(fi.Mode().IsRegular() || fi.Mode().IsDir()) {
			return nil
		}

		// create a new dir/file header
		header, err := tar.FileInfoHeader(fi, fi.Name())
		if err != nil {
			return err
		}

		// makes file path relative to srcPath
		header.Name = filepath.Join(dstPath, strings.TrimPrefix(strings.Replace(file, srcPath, "", -1), string(filepath.Separator)))

		// Set uid & gid if enabled
		if opts.SetOwner {
			header.Uid, header.Gid = opts.UID, opts.GID
		}

		// write the header
		if err := tw.WriteHeader(header); err != nil {
			return err
		}

		// return on directory
		if fi.Mode().IsDir() {
			return nil
		}

		// open files for taring
		f, err := os.Open(file)
		if err != nil {
			return err
		}
		defer f.Close()

		// copy file data into tar writer
		if _, err := io.Copy(tw, f); err != nil {
			return err
		}

		return nil
	})
}
