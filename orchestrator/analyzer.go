package orchestrator

import (
	"archive/tar"
	"context"
	"encoding/json"
	"io"
	"log"
	"os"
	"path"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/jsonmessage"
	"github.com/docker/docker/pkg/stdcopy"
	"github.com/docker/docker/pkg/term"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

const (
	DefaultArtifactDir = "/tmp"     // where the artifact is written by default
	DefaultTargetDir   = "/tmp/app" // where the source directory is mounted or copied by default
)

// ImportStrategy describes a way to import the target directory in a Docker container.
type ImportStrategy int

const (
	ImportMount ImportStrategy = iota // ImportMount instructs to mount the source dir
	ImportCopy                        // ImportCopy instructs to copy the source dir
)

// AnalyzerOptions configures the analyzer.
type AnalyzerOptions struct {
	ArtifactDir    string
	TargetDir      string
	ImportStrategy ImportStrategy
	tarOptions
}

// Analyzer is a Docker-base analyzer.
type Analyzer struct {
	cli          *client.Client
	imageRef     string
	artifactName string
	AnalyzerOptions
}

// NewAnalyzer initializes a new analyzer.
func NewAnalyzer(cli *client.Client, imageRef, artifactName string, opts AnalyzerOptions) *Analyzer {

	// apply default values
	if opts.ArtifactDir == "" {
		opts.ArtifactDir = DefaultArtifactDir
	}
	if opts.TargetDir == "" {
		opts.TargetDir = DefaultTargetDir
	}

	return &Analyzer{
		cli:             cli,
		imageRef:        imageRef,
		artifactName:    artifactName,
		AnalyzerOptions: opts,
	}
}

// Pull pulls the Docker image of the analyzer.
func (a Analyzer) Pull(ctx context.Context) error {

	// Pull Docker image
	r, err := a.cli.ImagePull(ctx, a.imageRef, types.ImagePullOptions{})
	if err != nil {
		return err
	}
	defer r.Close()

	// Display output of image pull to stderr
	termFd, isTerm := term.GetFdInfo(os.Stderr)
	if err := jsonmessage.DisplayJSONMessagesStream(r, os.Stderr, termFd, isTerm, nil); err != nil {
		return err
	}

	return nil
}

// Run runs the analyzer on the given directory and returns the resulting report.
func (a Analyzer) Run(ctx context.Context, sourceDir string) (*issue.Report, error) {
	cid, err := a.runContainer(ctx, sourceDir)
	if err != nil {
		return nil, err
	}

	// remove container on exit
	defer func() {
		opts := types.ContainerRemoveOptions{RemoveVolumes: true, Force: true}
		if err := a.cli.ContainerRemove(ctx, cid, opts); err != nil {
			log.Println(err)
		}
	}()

	return a.parseReport(ctx, cid)
}

// runContainer creates and starts a Docker container.
// It copies or mounts the source directory depending on the import strategy.
func (a Analyzer) runContainer(ctx context.Context, sourceDir string) (containerId string, err error) {

	// Configure according to import strategy
	mounts := []mount.Mount{}
	prepare := func(cid string) error { return nil }

	if a.ImportStrategy == ImportMount {
		// Mount target directory
		mounts = append(mounts, mount.Mount{
			Type:     mount.TypeBind,
			Source:   sourceDir,
			Target:   a.TargetDir,
			ReadOnly: false,
		})
	} else {
		// Copy prior to starting the container
		prepare = func(cid string) error {
			// Copy project directory to container using a tar archive.
			// Destination path must exist but directories listed in the tar archive are automatically created,
			// so this is why targetDir is used as a prefix when adding the files to the archive.
			r, w := io.Pipe()
			topts := a.tarOptions
			go func() {
				if err := writeTar(sourceDir, a.TargetDir, w, topts); err != nil {
					log.Println(err)
				}
				w.Close()
			}()
			copyOpts := types.CopyToContainerOptions{CopyUIDGID: false}
			return a.cli.CopyToContainer(ctx, cid, "/", r, copyOpts)
		}
	}

	// Environment
	env := []string{
		command.EnvVarTargetDir + "=" + a.TargetDir,
		command.EnvVarArtifactDir + "=" + a.ArtifactDir,
	}
	for _, e := range os.Environ() {
		switch strings.SplitN(e, "=", 2)[0] {
		case "PATH", "TMPDIR":
			// don't propagate
		default:
			// propagate
			env = append(env, e)
		}
	}

	// Container config
	cfg := &container.Config{
		Image: a.imageRef,
		Env:   env,
	}

	// Container host config
	hostCfg := &container.HostConfig{
		Mounts: mounts,
	}

	// Create container
	containerName := "" // use automatic naming to avoid conflicting names
	resp, err := a.cli.ContainerCreate(ctx, cfg, hostCfg, nil, containerName)
	if err != nil {
		return "", err
	}

	// Copy logs on exit
	defer func() {
		logsOpts := types.ContainerLogsOptions{
			ShowStdout: true,
			ShowStderr: true,
			Follow:     true,
		}
		if reader, err := a.cli.ContainerLogs(ctx, resp.ID, logsOpts); err != nil {
			log.Println(err)
		} else {
			if _, err := stdcopy.StdCopy(os.Stderr, os.Stderr, reader); err != nil {
				log.Println(err)
			} else {
				reader.Close()
			}
		}
	}()

	// Prepare container before starting
	if err := prepare(resp.ID); err != nil {
		return resp.ID, err
	}

	// Start image
	if err := a.cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		return resp.ID, err
	}

	// Wait for the analyzer
	statusCh, errCh := a.cli.ContainerWait(ctx, resp.ID, container.WaitConditionNextExit)
	select {
	case err := <-errCh:
		if err != nil {
			return resp.ID, err
		}
	case waitOK := <-statusCh:
		if waitOK.Error != nil {
			return resp.ID, err
		}
		switch waitOK.StatusCode {
		case 0:
			// successful
		case 3:
			return resp.ID, ErrNotFound
		default:
			return resp.ID, ErrNonZeroStatus
		}
	}

	return resp.ID, nil
}

// parseReport extracts and parses the report generated by given container.
func (a Analyzer) parseReport(ctx context.Context, containerId string) (*issue.Report, error) {
	// Create tar archive for parent directory
	archive, _, err := a.cli.CopyFromContainer(ctx, containerId, a.ArtifactDir)
	if err != nil {
		return nil, err
	}
	defer archive.Close()

	// path has no leading slash in archive header
	artifactPath := strings.TrimLeft(path.Join(a.ArtifactDir, a.artifactName), "/")

	// Browse archive, find and parse artifact
	tr := tar.NewReader(archive)
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		if hdr.Name == artifactPath {
			// decode JSON document
			var report issue.Report
			if err := json.NewDecoder(tr).Decode(&report); err != nil {
				return nil, err
			}
			return &report, nil
		}
	}

	return nil, ErrNoArtifact
}
